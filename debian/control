Source: zmk
Section: devel
Priority: optional
Maintainer: Zygmunt Krynicki <me@zygoon.pl>
Standards-Version: 4.5.1
Homepage: https://github.com/zyga/zmk
Build-Depends: debhelper-compat (= 13)
Vcs-Git: https://salsa.debian.org/zyga-guest/zmk.git
Vcs-Browser: https://salsa.debian.org/zyga-guest/zmk
Rules-Requires-Root: no

Package: zmk
Section: libs
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, make
Description: collection of reusable Makefiles
 Collection of makefiles implementing a system similar to autotools, but
 without the generated files that make understanding system behaviour harder.
 .
 Highlights include:
 .
  - Describe programs, test programs, static libraries, shared libraries,
    development headers, manual pages and more
  - Use familiar targets like "all", "check", "install" and "clean"
  - Works out of the box on popular distributions of Linux and MacOS
  - Friendly to distribution packaging expecting autotools
  - Compile natively with gcc, clang, tcc or the open-watcom compilers
  - Cross compile with gcc and open-watcom
  - Efficient and incremental, including the install target

Package: zmk-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: collection of reusable Makefiles (manual pages)
 Collection of makefiles implementing a system similar to autotools, but
 without the generated files that make understanding system behaviour harder.
 .
 Highlights include:
 .
  - Describe programs, test programs, static libraries, shared libraries,
    development headers, manual pages and more
  - Use familiar targets like "all", "check", "install" and "clean"
  - Works out of the box on popular distributions of Linux and MacOS
  - Friendly to distribution packaging expecting autotools
  - Compile natively with gcc, clang, tcc or the open-watcom compilers
  - Cross compile with gcc and open-watcom
  - Efficient and incremental, including the install target
 .
 This package contains the manual pages for zmk.
